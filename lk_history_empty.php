<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_history.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_history_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block10">
				<div class="center column">
					<div class="block10__row empty_page flex column">
						<p>История заказов</p>
					</div>
					<div class="block10_empty">
						<img src="assets/images/empty.svg">
						<p>Список заказов пуст. Создайте заявку и она отразится в истории заказов</p>
						<button type="button" class="flex yellow">Создать заявку</button>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>