<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_history.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_history_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block10">
				<div class="center column">
					<div class="block10__row flex column">
						<p>История заказов</p>
						<div class="flex">
							<div>
								<p>Транспортная компания</p>
								<select id="company">
									<option>Option 1</option>
									<option>Option 2</option>
									<option>426000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</option>
								</select>
							</div>
							<div>
								<p>Дата забора</p>
								<div class="flex">
									<div class="input_item zab_from"><input id="zab_from" type="text" name="zab_from"><label for="zab_from">От</label></div>
									<div class="input_item zab_to"><input id="zab_to" type="text" name="zab_to"><label for="zab_to">До</label></div>
								</div>
							</div>
							<div>
								<p>Номер накладной</p>
								<div class="input_item"><input id="nakl_numb" type="text"><label for="nakl_numb">Пример, RU180312-145877 </label></div>
							</div>
						</div>
					</div>
					<div class="block10_list">
						<div class="project canceled">
							<div class="block10__project_row">
								<p>Заказ № RU180312-145877</p>
								<p class="label">Отменен</p>
							</div>
							<div class="block10__project_body flex column">
								<div class="flex">
									<div class="arrow_body flex column">
										<p><span>адрес отправления</span>426000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
										<p><span>адрес отправления</span>426000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
									</div>
									<div class="info_body flex column">
										<ul>
											<li><p><span>ТК</span>Major Delivery</p></li>
											<li><p><span>Номер накладной</span>426000426000</p></li>
											<li><p><span>дата забора</span>20.10.2020</p></li>
											<li><p><span>дата доставки</span>27.10.2020</p></li>
										</ul>
										<p>488 554 руб.</p>
									</div>
								</div>
								<div class="block10__project_buttons flex">
									<button type="button" class="flex blue"><img src="assets/images/block10_download.svg">скачать накладную</button>
									<button type="button" class="flex yellow">подробнее о заказе</button>
								</div>
							</div>
						</div>
						<div class="project done">
							<div class="block10__project_row">
								<p>Заказ № RU180312-145877</p>
								<p class="label">Доставлен</p>
							</div>
							<div class="block10__project_body flex column">
								<div class="flex">
									<div class="arrow_body flex column">
										<p><span>адрес отправления</span>426000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
										<p><span>адрес отправления</span>426000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
									</div>
									<div class="info_body flex column">
										<ul>
											<li><p><span>ТК</span>Major Delivery</p></li>
											<li><p><span>Номер накладной</span>426000426000</p></li>
											<li><p><span>дата забора</span>20.10.2020</p></li>
											<li><p><span>дата доставки</span>27.10.2020</p></li>
										</ul>
										<p>488 554 руб.</p>
									</div>
								</div>
								<div class="block10__project_buttons flex">
									<button type="button" class="flex blue"><img src="assets/images/block10_download.svg">скачать накладную</button>
									<button type="button" class="flex yellow">подробнее о заказе</button>
								</div>
							</div>
						</div>
						<div class="project sended">
							<div class="block10__project_row">
								<p>Заказ № RU180312-145877</p>
								<p class="label">Отправлен</p>
							</div>
							<div class="block10__project_body flex column">
								<div class="flex">
									<div class="arrow_body flex column">
										<p><span>адрес отправления</span>426000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
										<p><span>адрес отправления</span>426000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
									</div>
									<div class="info_body flex column">
										<ul>
											<li><p><span>ТК</span>Major Delivery</p></li>
											<li><p><span>Номер накладной</span>426000426000</p></li>
											<li><p><span>дата забора</span>20.10.2020</p></li>
											<li><p><span>дата доставки</span>27.10.2020</p></li>
										</ul>
										<p>488 554 руб.</p>
									</div>
								</div>
								<div class="block10__project_buttons flex">
									<button type="button" class="flex blue"><img src="assets/images/block10_download.svg">скачать накладную</button>
									<button type="button" class="flex yellow">подробнее о заказе</button>
								</div>
							</div>
						</div>
					</div>
					<div class="block10__pagination flex">
						<button type="button" class="flex blue">Следующая страница</button>
						<ul class="flex">
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li>...</li>
							<li><a href="#">10</a></li>
						</ul>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>