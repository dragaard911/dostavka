function init() {
    var myPlacemark,
        myMap = new ymaps.Map('map', {
            center: [50.54722726916766,137.00779745220038],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        });

    myMap.events.add('click', function (e) {
        var coords = e.get('coords');

        if (myPlacemark) {
            myPlacemark.geometry.setCoordinates(coords);
        }
        else {
            myPlacemark = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark);
            myPlacemark.events.add('dragend', function () {
                getAddress(myPlacemark.geometry.getCoordinates());
            });
        }
        getAddress(coords);
    });
	
	$(".popup_maps .popup_buttons button").on('click',function(){
		myMap.destroy();
	});

    function createPlacemark(coords) {
        return new ymaps.Placemark(coords, {}, {
			iconLayout: 'default#image',
			iconImageHref: 'assets/images/map_point.png',
			iconImageSize: [64, 77],
			iconImageOffset: [-32, -77],
            preset: 'islands#violetDotIconWithCaption',
            draggable: true
        });
    }
	
	function setAdressLine(placemarkText){
		$(".popup_maps #adress_line").html(placemarkText.getAddressLine());
		$(".popup_block__fullwidth.popup_maps .popup_buttons").css('height','auto');
		$(".popup_block__fullwidth .popup_buttons .yellow.inactive").removeClass('inactive');
	}

    function getAddress(coords) {
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);
			setAdressLine(firstGeoObject);
        });
    }
}
