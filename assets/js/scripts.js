function open_form(form)
{
	$(".popup_block__fullwidth, .popup_block__form").fadeOut(0);
	$("body").css({'overflow':'hidden','width':document.documentElement.clientWidth});
	$(".popup_block, .popup_block__bg").fadeIn(100);
	$("."+form).fadeIn(100);
	$("."+form).find('form').fadeIn(0);
	$("."+form).find('.result').fadeOut(0);
}

function map_resize()
{
	let result_height = 0;
	let top_padding = ($('.popup_maps').css('padding-top')).split('px');
	let buttons_height = ($('.popup_maps .popup_buttons').css('height')).split('px');
	let p_height = ($('.popup_maps .center p').css('line-height')).split('px');
	let map_margin = ($(".popup_block__fullwidth .popup_map").css('margin-top')).split('px');
	
	result_height = $(window).height() - parseInt(top_padding[0]) - parseInt(buttons_height[0]) - parseInt(p_height[0]) - parseInt(map_margin[0]);
	
	$(".popup_map").css('height', result_height+'px');
}

$(window).resize(function(){
	map_resize();
});

$(document).ready(function(){
	
	map_resize();
	
	$("header.header_lk .header__menu button.burger").on('click',function(){
		open_form('header__menu_mobile');
	});
	
	$(".block2__adress_block_adresses > div .input_container div").on('input',function(){
		$(this).parents('.input_container').find('input').val($(this).html());
		$(this).parents('.block2__adress_block_container').addClass('error');
	});

	
	$(".block2__adress_block_adresses > div .input_container div").on('focusin',function(){
		
		$('div.adress_list').removeClass('active');
		$(this).parents('.block2__adress_block_container').find('.recent').addClass('active');
		
		if(!$(this).html())
		{	
			$(this).parents('.input_container').find('label').addClass('label_stop');
			$(this).parents('.input_container').addClass('active');
		}
	});
	
	$(".block2__adress_block_adresses > div .input_container div").on('focusout',function(){
		
		if(!$(this).html())
		{	
			$(this).parents('.input_container').find('label').removeClass('label_stop');
			$(this).parents('.input_container').removeClass('active');
			$(this).parents('.block2__adress_block_container').removeClass('error');
			$(this).parents('.block15__reg_block').removeClass('done');
		}
	});
	
	$("div.adress_list ul li").on('click',function(){
		$(this).parents('.block2__adress_block_container').find('.input_container div').html($(this).find('span').html());
		$(this).parents('.block2__adress_block_container').find('.input_container input').val($(this).find('span').html());
		$(this).parents('.block2__adress_block_container').removeClass('error');
		$(this).parents('.adress_list').removeClass('active');
		$(this).parents('.block2__adress_block_container').find('.input_container label').addClass('label_stop');
	});
	
	$(document).on('click',function(event){
		target = $(event.target);

		if(!target.closest('.block2__adress_block_container').length)
			$('.block2__adress_block_container').find('.adress_list').removeClass('active');
	});
	
	$(".block2 div.adress_list .shitchbook").on('click',function(){
		$('div.adress_list').removeClass('active');
		$(this).parents('.block2__adress_block_container').find('.adress_list.book').addClass('active');
	});
	
	$(".block2__adress_block_adresses > div .input_container label").on('click',function(){
		if($(this).attr('for'))
			$("div#"+$(this).attr('for')).focus();
	});
	
	$(".block3__columns_list li > label").on('click',function(){
		if(!$(this).parents('li').hasClass('opened'))
		{
			$(this).parents('li').addClass('active');
			$(this).parents('li').find('div').slideDown(300, function(){$(this).parents('li').addClass('opened');});
		}
		else
		{
			$(this).parents('li').removeClass('active');
			$(this).parents('li').find('div').slideUp(300, function(){$(this).parents('li').removeClass('opened');});
		}
	});
	
	$("div.popup_block__form button.white.close span, div.popup_block__form button.blue.close").on('click', function(){
		$(this).parents('.popup_block__form').fadeOut(100,function(){$("body").css({'overflow':'visible','width':'auto'});});
		$(this).parents('.popup_block').find('.popup_block__bg').fadeOut(100);	
		$(".popup_block").fadeOut(100);
	});
	
	$(".block3__columns_info p a").on('click', function(e){
		e.preventDefault();
		open_form('popup_block__feedback');
	});
	
	$("div.popup_block__bg, header .mobile_row img").on('click', function(){
		$('div.popup_block__form, div.popup_block__bg, .header__menu_mobile, .popup_block').fadeOut(100,function(){$("body").css({'overflow':'visible','width':'auto'});});
	});
	
	$("div.popup_block__form button.send").on('click', function(){
		$(this).parents('.popup_block__form').find('form').fadeOut(0);
		$(this).parents('.popup_block__form').find('.result').fadeIn(100);
	});
	
	$(".footer__info > div:nth-child(1) .phone_form a").on('click', function(element){
		element.preventDefault();
		open_form('popup_block__phonecall');
	});
	
	$(".footer__info > div:nth-child(1) button").on('click', function(){
		open_form('popup_block__feedbac');
	});
	
	$(".popup_block__fullwidth.popup_maps .popup_buttons .yellow").on('click', function(){
		$(".adress_completion").find('.input_container label').addClass('label_stop');
		$(".adress_completion").find('.input_container div').html($(".popup_maps #adress_line").html());
		$(".adress_completion").find('.input_container input').val($(".popup_maps #adress_line").html());
		$(".adress_completion").removeClass("adress_completion");
	});
	
	// *** Offer page *** //
	
	$(".block6__results button").on('click',function(){
		
		if(!$(this).hasClass('open') && !$(this).hasClass('stop'))
		{
			$(this).addClass('open stop');
			$(".block6__table").slideDown(300, function(){$(".block6__results button").removeClass('stop')});
		}
		else
		{
			$(this).removeClass('open');
			$(".block6__table").slideUp(300);
		}
			
	});
	
	// *** Enter page *** //
	
	$("#enter #email_enter, #enter #passwd_enter").on('keyup',function(){
		if($("#enter #email_enter").val() && $("#enter #passwd_enter").val())
			$("#enter .inactive").removeClass('inactive');
	});
	
	$("#enter .yellow").on('click',function(){
		if(!$(this).hasClass('inactive'))
		{
			$(this).parents('#enter').find('.form').fadeOut(1);
			$(this).parents('#enter').find('.form_success').fadeIn(300);
		}
	});
	
	$("#registr #email_registr, #registr #passwd_registr").on('keyup',function(){
		if($("#registr #email_registr").val() && $("#registr #passwd_registr").val())
			$("#registr .inactive").removeClass('inactive');
	});
	
	$("#registr .yellow").on('click',function(){
		if(!$(this).hasClass('inactive'))
		{
			$(this).parents('#registr').find('.form').fadeOut(1);
			$(this).parents('#registr').find('.form_success').fadeIn(300);
		}
	});
	
	
	$("#pass_restore #email_restore").on('keyup',function(){console.log($(this).val());
		if($(this).val())
			$("#pass_restore .inactive").removeClass('inactive');
	});
	
	$("#pass_restore .yellow").on('click',function(){
		if(!$(this).hasClass('inactive'))
		{
			$(this).parents('#pass_restore').find('.form').fadeOut(1);
			$(this).parents('#pass_restore').find('.form_success').fadeIn(300);
		}
	});
	
	// *** Lk_profile *** //
	
	$(".block9__info .input_block .check_string").on('click',function(){
		if($(this).find('input').prop('checked'))
			$(".adress_copy").fadeOut(1);
		else
			$(".adress_copy").fadeIn(300);
	});
	
	// *** Lk_history *** //
	
	$("#company, #get_time").selectmenu();
	
	$("#date_from, #date_to, #get_date, #zab_from, #zab_to").datepicker({
		dateFormat : "dd.mm.yy",
		minDate: new Date($('#hiddendelivdate').val()),
		monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		dayNamesMin : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
	});
	
	// *** LK profile *** //
	
	$(".block9__info .input_block .pass_change").on('click',function(){
		open_form('popup_block__pass');
	});
	
	$(".block9__info .input_block .buttons button").on('click',function(){
		let status_string = $(this).parents('.buttons').find('.status');
		status_string.addClass('success');
		setTimeout(function(){status_string.removeClass('success');}, 5000);
	});
	
	$("#get_date").on('change',function(){
		if($(this).val())
			$(this).parents('.input_item').addClass('focused');
	});
	
	$("div.input_item input, div.input_item textarea").on('focusin',function(){
		$(this).parents('.input_item').addClass('active');
		if(!$(this).val())
			$(this).parents('.input_item').addClass('focused');
	});

	$("div.input_item input, div.input_item textarea").on('focusout',function(){
		$(this).parents('.input_item').removeClass('active');
		if(!$(this).val())
			$(this).parents('.input_item').removeClass('focused');
	});
	
	$(".popup_block__pass .check_pass").on('click',function(){
		$(this).parents('.input_item').find('input').attr('type','text');
		$(this).fadeOut(1);
		$(this).parents('.input_item').find(".hide_pass").fadeIn(1);
	});
	
	$(".popup_block__pass .hide_pass").on('click',function(){
		$(this).parents('.input_item').find('input').attr('type','password');
		$(this).fadeOut(1);
		$(this).parents('.input_item').find(".check_pass").fadeIn(1);
	});
	
	// *** Lk_balance *** //
	
	$(".block12__balance_row button").on('click',function(){
		open_form('popup_block__bill');
	});
	
	// *** LK_registr *** //
	
	$(".block13__info .input_block .check_string").on('click',function(){
		if($(this).find('input').prop('checked'))
			$(".adress_copy").fadeOut(1);
		else
			$(".adress_copy").fadeIn(300);
	});
	
	$(".block13 #reg_passwd").on('keyup',function(){
		if(($(this).val()).length < 6)
			$(".passwd_difficult").removeClass('strong').addClass('weak').fadeIn(1).find('span').html('Низкий');
		else if(($(this).val()).length >= 6)
			$(".passwd_difficult").removeClass('weak').addClass('strong').fadeIn(1).find('span').html('Высокий');
	});
	
	$(".block13 #reg_passwd_re, .block13 #reg_passwd").on('keyup',function(){console.log($(this).val());
		if($(".block13 #reg_passwd_re").val() != $(".block13 #reg_passwd").val())
			$(".passwd_difference").removeClass('strong').addClass('weak').fadeIn(1).find('span').html('Пароли не совпадают');
		else 
			$(".passwd_difference").removeClass('weak').addClass('strong').fadeIn(1).find('span').html('Пароли совпадают');
	});
	
	$(".block13__info .input_block .check_pass").on('click',function(){
		$(this).parents('.input_item').find('input').attr('type','text');
		$(this).fadeOut(1);
		$(this).parents('.input_item').find(".hide_pass").fadeIn(1);
	});
	
	$(".block13__info .input_block .hide_pass").on('click',function(){
		$(this).parents('.input_item').find('input').attr('type','password');
		$(this).fadeOut(1);
		$(this).parents('.input_item').find(".check_pass").fadeIn(1);
	});
	
	// *** Confirm *** //
	
	$(".block8 button.yellow").on('click',function(){
		open_form('popup_block__order');
	});
	
	// *** Popups *** //
	
	$(".popup_block__fullwidth .popup_table .popup_table_td").on('click',function(){
		$(".popup_block__fullwidth .popup_table .popup_table_td").removeClass('clicked');
		$(this).addClass('clicked');
		$(".popup_block__fullwidth .popup_buttons .yellow").removeClass('inactive');
	});
	
	$(".popup_block__fullwidth .popup_buttons .white").on('click',function(){
		$(".popup_block, .popup_block__bg, .popup_block__fullwidth").fadeOut(100,function(){$("body").css({'overflow':'visible','width':'auto'});});
		$(".popup_block__fullwidth .popup_buttons .yellow").addClass('inactive');
		$(".popup_block__fullwidth .popup_table .popup_table_td").removeClass('clicked');
	});
	
	$(".popup_block__fullwidth .popup_buttons .yellow").on('click',function(){
		$(".popup_block, .popup_block__bg, .popup_block__fullwidth").fadeOut(100,function(){$("body").css({'overflow':'visible','width':'auto'});});
		$(".popup_block__fullwidth .popup_buttons .yellow").addClass('inactive');
		$(".popup_block__fullwidth .popup_table .popup_table_td").removeClass('clicked');
	});
	
	$("button.check_list").on('click',function(){
		open_form('popup_adress');
	});
	
	$("button.map_list").on('click',function(){
		$(this).parents('.block2__adress_block_container').addClass('adress_completion');
		open_form('popup_maps');
		ymaps.ready(init);
	});
	
	// *** Lk_history_order *** //
	
	$(".block11__info .delivery .flex button").on('click',function(){
		open_form('popup_block__nakl');
	});
	
	// *** Lk_order *** //
	
	function check_fields(form, form_data)
	{
		let stoper = 0;

		for(var pair of form_data.entries()) {
		   if(!pair[1])
			stoper = 1;
		}

		if(stoper == 0)
			$("#"+form).parents('.block15__reg_block').addClass('done');
		else
			$("#"+form).parents('.block15__reg_block').removeClass('done');
	}
	
	$(".block15 .adress_list ul li").on('click',function(){
		let steps = new FormData(document.forms.step1);
		check_fields('step1', steps);
	});
	
	$("#step2 input,#step2 select").on('change',function(){
		let steps2 = new FormData(document.forms.step2);
		check_fields('step2', steps2);
	});
	
	$("#step3 input").on('change',function(){
		let steps3 = new FormData(document.forms.step3);
		check_fields('step3', steps3);
	});
	
	$("#step4 input").on('change',function(){
		let steps4 = new FormData(document.forms.step4);
		check_fields('step4', steps4);
	});
	
	$(".adress_book_buttons .add_adress").on('click',function(e){
		e.preventDefault();
		open_form('popup_block__contact');
	});
	
	$(".adress_book_buttons .get_adress").on('click',function(e){
		e.preventDefault();
		open_form('popup_contacts');
	});
	
	$(".popup_block__fullwidth .popup_table .popup_table_td div:last-child a:first-child").on('click',function(e){
		e.preventDefault();
		open_form('popup_block__contact_change');
	});
	
	$(".popup_block__fullwidth .popup_table .popup_table_td div:last-child a:last-child").on('click',function(e){
		e.preventDefault();
		open_form('popup_block__del_contact');
	});
	
	// *** Lk_registr *** //
	
	function check_reg_fields(form, form_data)
	{
		let stoper = 0;

		for(var pair of form_data.entries()) {
		   if(!pair[1])
			stoper = 1;
		}
		
		if(stoper == 0)
			$("#"+form).parents('.block13__reg_block').addClass('done');
		else
			$("#"+form).parents('.block13__reg_block').removeClass('done');
	}
	
	$("#reg_step1 input").on('change',function(){
		let steps1 = new FormData(document.forms.reg_step1);
		check_reg_fields('reg_step1', steps1);
	});
	
	$("#reg_step2 input").on('change',function(){
		let steps2 = new FormData(document.forms.reg_step2);
		check_reg_fields('reg_step2', steps2);
	});
	
	$("#reg_step3 input").on('change',function(){
		let steps3 = new FormData(document.forms.reg_step3);
		check_reg_fields('reg_step3', steps3);
	});
	
	$("button.add_book").on('click',function(){
		open_form('popup_block__adress');
	});
});