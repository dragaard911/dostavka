			<header>
				<div class="center column">
					<div class="header__menu flex">
						<a href="#"><img src="assets/images/header_logo.svg"></a>
						<button type="button" class="flex black"><img src="assets/images/button_key.svg">вход</button>
					</div>
					<div class="header__row flex column">
						<p>Капитан Доставка найдет самые быстрые и эффективные курьерские службы для ваших отправлений</p>
						<button type="button" class="flex black"><img src="assets/images/button_arrow_right.svg">регистрация</button>
					</div>
				</div>
			</header>