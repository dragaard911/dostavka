			<header class="header_lk">
				<div class="center column">
					<div class="header__menu flex">
						<a href="#"><img src="assets/images/header_logo_lk.svg"></a>
						<button type="button" class="flex yellow"><img src="assets/images/header_plus.svg">Новая заявка</button>
						<button type="button" class="flex white">История заказов</button>
						<button type="button" class="flex white">Баланс</button>
						<button type="button" class="flex white">Константин<img src="assets/images/header_exit.svg"></button>
						<button type="button" class="flex white burger"><img src="assets/images/header_burger.svg"></button>
					</div>
				</div>
				<div class="header__menu_mobile">
					<div class="center column">
						<div class="mobile_row">
							<img src="assets/images/header_close.svg">
						</div>
						<button type="button" class="flex yellow"><img src="assets/images/header_plus.svg">Новая заявка</button>
						<button type="button" class="flex white">История заказов</button>
						<button type="button" class="flex white">Баланс</button>
						<button type="button" class="flex white user">Константин<img src="assets/images/header_exit.svg"></button>
					</div>
				</div>
			</header>