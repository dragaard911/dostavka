<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/confirm.css" rel="stylesheet" type="text/css">
		<link href="assets/css/confirm_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block8">
				<div class="center column">
					<div class="block8__row flex column">
						<p>Подтверждение заказа</p>
						<p>Проверьте все данные заказа и подтвердите его</p>
					</div>
					<div class="block8__info">
						<div class="flex sendrecieve">
							<div class="flex column">
								<p><img src="assets/images/block8_sender.svg">Отправитель</p>
								<div>
									<p><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
									<p><span>фио</span>Константинопольский Константин Константинович</p>
									<p><span>Телефон</span>+7 999 666 98 45</p>
									<p><span>Компания</span>Ростелеком</p>
									<p><span>дата и время забора</span>20 сентября, 10:00—20:00</p>
								</div>
							</div>
							<div class="flex column">
								<p><img src="assets/images/block8_recieve.svg">Получатель</p>
								<div>
									<p><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
									<p><span>фио</span>Константинопольский Константин Константинович</p>
									<p><span>Телефон</span>+7 999 666 98 45</p>
									<p><span>Компания</span>Yodiz Studio</p>
								</div>
							</div>
						</div>
						<div class="delivery">
							<p>Кто доставляет</p>
							<div class="flex">
								<div class="flex">
									<p><span>Транспортная компания</span>Байкал-Сервис Комсомольск-на-Амуре Логистик</p>
									<p><span>Срок доставки</span>8 дней</p>
									<p><span>Стоимость</span>478 555 руб.</p>
								</div>
							</div>
						</div>
						<div class="flex column goods">
							<p>Информация о грузе</p>
							<div class="flex column">
								<ul class="flex">
									<li><p><span>Вес</span>145,5 кг.</p></li>
									<li><p><span>ширина</span>185,5 см.</p></li>
									<li><p><span>Высота</span>185 см.</p></li>
									<li><p><span>глубина</span>20 см.</p></li>
								</ul>
								<p><span>Описание</span>Ящик деревянный, белого цвета, внутри хрупкое, соответствующие обозначения на упаковке есть.</p>
								<p><span>Комментарий</span>Позвонить за 1 час до прибытия на адрес</p>
							</div>
						</div>
						<button type="button" class="flex yellow">отправить заказ</button>
						<button type="button" class="flex blue">изменить заказ</button>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>