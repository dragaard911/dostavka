<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/offer.css" rel="stylesheet" type="text/css">
		<link href="assets/css/offer_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block6">
				<div class="center column">
					<div class="block6__row flex">
						<p>Результаты поиска</p>
						<button type="button" class="flex white"><img src="assets/images/block6_arrow.svg">Назад к расчету стоимости</button>
					</div>
					<div class="block6__offers flex">
						<div class="block6__offers_block fastest">
							<p>Самый быстрый</p>
							<div class="flex">
								<div class="flex column">
									<div class="flex offers_description">
										<div>
											<img src="assets/images/block6_box.svg">
											<img class="offer_icon" src="assets/images/block6_fastest.svg">
										</div>
										<div class="flex column">
											<p class="block6_offer_header">компания</p>
											<p>Байкал-Сервис Комсомольск-на-Амуре Логистик</p>
											<div class="flex offer_options">
												<div>
													<p class="block6_offer_header">Срок доставки</p>
													<p>8 дней</p>
												</div>
												<div>
													<p class="block6_offer_header">Дата забора</p>
													<p>10 сентября</p>
												</div>
												<div>
													<p class="block6_offer_header">Дата доставки</p>
													<p>18 сентября</p>
												</div>
											</div>
										</div>
									</div>
									<hr>
									<div class="flex offers_price">
										<div>
											<p class="block6_offer_header">Стоимость</p>
											<p>458 455 руб.</p>
										</div>
										<button type="button" class="flex black">оформить заявку</button>
									</div>
								</div>
							</div>
						</div>
						<div class="block6__offers_block cheap">
							<p>Самый экономичный</p>
							<div class="flex">
								<div class="flex column">
									<div class="flex offers_description">
										<div>
											<img src="assets/images/block6_box.svg">
											<img class="offer_icon" src="assets/images/block6_cheap.svg">
										</div>
										<div class="flex column">
											<p class="block6_offer_header">компания</p>
											<p>Байкал-Сервис Комсомольск-на-Амуре Логистик</p>
											<div class="flex offer_options">
												<div>
													<p class="block6_offer_header">Срок доставки</p>
													<p>8 дней</p>
												</div>
												<div>
													<p class="block6_offer_header">Дата забора</p>
													<p>10 сентября</p>
												</div>
												<div>
													<p class="block6_offer_header">Дата доставки</p>
													<p>18 сентября</p>
												</div>
											</div>
										</div>
									</div>
									<hr>
									<div class="flex offers_price">
										<div>
											<p class="block6_offer_header">Стоимость</p>
											<p>458 455 руб.</p>
										</div>
										<button type="button" class="flex yellow">оформить заявку</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="block6__results">
						<button type="button" class="flex"><img src="assets/images/block6_result.svg"><span class="hide">скрыть все результаты</span><span class="show">Смотреть все результаты</span></button>
					</div>
					<div class="block6__table">
						<div class="block6__table_body">
							<div class="row th">
								<div class="cell">компания</div>
								<div class="cell">Срок доставки</div>
								<div class="cell">Дата забора</div>
								<div class="cell">Дата доставки</div>
								<div class="cell">Стоимость</div>
								<div class="cell"></div>
							</div>
							<div class="row">
								<div class="cell"><p class="cell_mobile">компания</p>Байкал-Сервис Комсомольск-на-Амуре Логистик</div>
								<div class="cell"><p class="cell_mobile">Срок доставки</p>8 дней</div>
								<div class="cell"><p class="cell_mobile">Дата забора</p>10 сентября</div>
								<div class="cell"><p class="cell_mobile">Дата доставки</p>18 сентября</div>
								<div class="cell"><p class="cell_mobile">Стоимость</p>458 455 руб.</div>
								<div class="cell"><button type="button" class="flex blue">оформить заявку</button></div>
							</div>
							<div class="row">
								<div class="cell"><p class="cell_mobile">компания</p>Байкал-Сервис Комсомольск-на-Амуре Логистик</div>
								<div class="cell"><p class="cell_mobile">Срок доставки</p>8 дней</div>
								<div class="cell"><p class="cell_mobile">Дата забора</p>10 сентября</div>
								<div class="cell"><p class="cell_mobile">Дата доставки</p>18 сентября</div>
								<div class="cell"><p class="cell_mobile">Стоимость</p>458 455 руб.</div>
								<div class="cell"><button type="button" class="flex blue">оформить заявку</button></div>
							</div>
							<div class="row">
								<div class="cell"><p class="cell_mobile">компания</p>Байкал-Сервис Комсомольск-на-Амуре Логистик</div>
								<div class="cell"><p class="cell_mobile">Срок доставки</p>8 дней</div>
								<div class="cell"><p class="cell_mobile">Дата забора</p>10 сентября</div>
								<div class="cell"><p class="cell_mobile">Дата доставки</p>18 сентября</div>
								<div class="cell"><p class="cell_mobile">Стоимость</p>458 455 руб.</div>
								<div class="cell"><button type="button" class="flex blue">оформить заявку</button></div>
							</div>
							<div class="row">
								<div class="cell"><p class="cell_mobile">компания</p>Байкал-Сервис Комсомольск-на-Амуре Логистик</div>
								<div class="cell"><p class="cell_mobile">Срок доставки</p>8 дней</div>
								<div class="cell"><p class="cell_mobile">Дата забора</p>10 сентября</div>
								<div class="cell"><p class="cell_mobile">Дата доставки</p>18 сентября</div>
								<div class="cell"><p class="cell_mobile">Стоимость</p>458 455 руб.</div>
								<div class="cell"><button type="button" class="flex blue">оформить заявку</button></div>
							</div>
						</div>
						<div class="block6__table_pagination flex">
							<button type="button" class="flex blue">Следующая страница</button>
							<ul class="flex">
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li>...</li>
								<li><a href="#">10</a></li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<?php include('footer.php');?>
	</body>
</html>