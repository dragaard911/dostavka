<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_profile.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_profile_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block9">
				<div class="center column">
					<div class="block9__row flex column">
						<p>Личный кабинет</p>
					</div>
					<div class="block9__info flex column">
						<p>Контактные данные</p>
						<div class="flex column input_block">
							<div class="flex">
								<div class="input_item"><input id="prof_sname" type="text"><label for="prof_sname">Фамилия</label></div>
								<div class="input_item"><input id="prof_name" type="text"><label for="prof_name">Имя</label></div>
							</div>
							<div class="flex">
								<div class="input_item"><input id="prof_email" type="text"><label for="prof_email">E-mail</label></div>
								<div class="input_item"><input id="prof_phone" type="text"><label for="prof_phone">Телефон</label></div>
							</div>
							<div class="flex fullwidth buttons">
								<button type="button" class="flex yellow">Сохранить</button>
								<p class="status flex">Изменения успешно сохранены</p>
							</div>
						</div>
						<p>О компании</p>
						<div class="flex column input_block">
							<p>Сервис сам находит данные по организации - достаточно ввести ИНН, БИК банка и Р/С</p>
							<div class="flex">
								<div class="input_item"><input id="prof_inn" type="text"><label for="prof_inn">ИНН</label></div>
								<div class="input_item"><input id="prof_kpp" type="text"><label for="prof_kpp">КПП</label></div>
							</div>
							<div class="flex fullwidth bottom">
								<div class="input_item"><input id="prof_ogrn" type="text"><label for="prof_ogrn">ОГРН</label></div>
							</div>
							<div class="flex">
								<div class="input_item"><input id="prof_bik" type="text"><label for="prof_bik">Бик Банка</label></div>
								<div class="input_item"><input id="prof_bank" type="text"><label for="prof_bank">Банк</label></div>
							</div>
							<div class="flex bottom">
								<div class="input_item"><input id="prof_ras_bill" type="text"><label for="prof_ras_bill">Рас. счет</label></div>
								<div class="input_item"><input id="prof_kor_bill" type="text"><label for="prof_kor_bill">Корр. счет</label></div>
							</div>
							<div class="flex fullwidth">
								<div class="input_item"><input id="prof_comp" type="text"><label for="prof_comp">Название компании</label></div>
							</div>
							<div class="flex fullwidth">
								<div class="input_item"><input id="prof_director" type="text"><label for="prof_director">Генеральный директор</label></div>
							</div>
							<div class="flex fullwidth adress_original">
								<div class="input_item"><input id="prof_com_adress" type="text"><label for="prof_com_adress">Юридический адрес</label></div>
							</div>
							<div class="flex">
								<p class="check_string"><input type="checkbox" id="prof_adress_copy" checked><label for="prof_adress_copy"><span><img src="assets/images/form_checkbox.svg"></span>Совпадает с фактическим</label></p>
							</div>
							<div class="flex fullwidth adress_copy">
								<div class="input_item"><input id="prof_usr_adress" type="text"><label for="prof_usr_adress">Фактический адрес</label></div>
							</div>
							<div class="flex fullwidth buttons">
								<button type="button" class="flex yellow">Сохранить</button>
								<p class="status flex">Изменения успешно сохранены</p>
							</div>	
						</div>
						<div class="flex column input_block">
							<button type="button" class="flex yellow pass_change">Изменить пароль</button>
						</div>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>