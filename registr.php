<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/enter.css" rel="stylesheet" type="text/css">
		<link href="assets/css/enter_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk_noauth.php');?>
			<section class="block7">
				<div class="center column">
					<div class="block7__row flex">
						<p>Регистрация</p>
					</div>
					<div class="block7__form flex column">
						<form id="registr">
							<div class="form">
								<div class="input_item"><input id="email_registr" type="text"><label for="email_registr">Электронная почта</label></div>
								<div class="input_item"><input id="passwd_registr" type="password" autocomplete="off"><label for="passwd_registr">Пароль</label></div>
								<button type="button" class="flex yellow inactive">регистрация</button>
								<button type="button" class="flex white">отмена</button>
							</div>
							<div class="form_success">
								<img src="assets/images/form_success.svg">
								<p>Письмо отправлено. Следуйте указанным в нем инструкциям</p>
								<button type="button" class="flex yellow">войти</button>
							</div>
						</form>
					</div>
				</div>
				<img class="block7__bg" src="assets/images/block7_bg.svg">
			</section>	
			<?php include('footer.php');?>
	</body>
</html>