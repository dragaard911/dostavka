<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_request.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_request_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
		<script src="https://api-maps.yandex.ru/2.1/?apikey=caaff12d-84d6-41c7-b12f-8e434af75542&lang=ru_RU" type="text/javascript"></script>
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block14">
				<div class="center column">
					<div class="block14__row flex">
						<p>Новая заявка</p>
					</div>
					<p>Адреса</p>
					<div class="block2__adress_block flex column">
						
							<div class="block2__adress_block_adresses flex column">
								<div class="block2__adress_block_container">
									<div class="block2__border_block flex">
										<div class="input_container">
											<label for="from">Адрес отправления полностью<span class="error_text">Неверный адрес</span></label>
											<div contenteditable="true" id="from"></div>
											<input type="hidden" name="from">
										</div>
										<div class="buttons flex">
											<button type="button" class="check_list"><img src="assets/images/list.svg"></button>
											<button type="button" class="map_list"><img src="assets/images/point.svg"></button>
										</div>
									</div>
									<div class="adress_list recent">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
											<button type="button" class="flex white shitchbook check_list"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
											<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
									<div class="adress_list book">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
											<button type="button" class="flex white shitchbook check_list"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
											<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
								</div>
								<div class="separate"><hr></div>
								<div class="block2__adress_block_container">
									<div class="block2__border_block flex">
										<div class="input_container">
											<label for="to">Адрес отправления полностью<span class="error_text">Неверный адрес</span></label>
											<div contenteditable="true" id="to"></div>
											<input type="hidden" name="to">
										</div>
										<div class="buttons flex">
											<button type="button" class="check_list"><img src="assets/images/list.svg"></button>
											<button type="button" class="map_list"><img src="assets/images/point.svg"></button>
										</div>
									</div>
									<div class="adress_list recent">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
											<button type="button" class="flex white check_list"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
											<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
									<div class="adress_list book">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
											<button type="button" class="flex white check_list"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
											<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<p>Информация о грузе</p>
						<div class="block2__delivery_block">
							<p>Стоимость будет точнее, если заполнить габариты груза</p>
							<div class="block2__delivery_block__info flex">
								<div>
									<p>Вес груза<span>кг</span></p>
									<div class="input_container">
										<div class="input_item"><input id="weight" type="text"><label for="weight">100</label></div>
									</div>
								</div>
								<div>
									<p>габариты груза<span>см</span></p>
									<div class="input_container input33">
										<div class="input_item"><input id="width" type="text"><label for="width">Ширина</label></div>
										<div class="input_item"><input id="height" type="text"><label for="height">Высота</label></div>
										<div class="input_item"><input id="depth" type="text"><label for="depth">Глубина</label></div>
									</div>
								</div>
							</div>
						</div>
						<button type="button" class="flex yellow"><img src="assets/images/button_box.svg">найти лучшее предложение</button>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>