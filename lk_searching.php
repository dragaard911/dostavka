<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_searching.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_searching_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block14">
				<div class="center column">
					<div id="lottie"></div>
					<p>Капитан Доставка ищет для вас лучшие варианты</p>
				</div>
			</section>	
			<?php include('footer.php');?>
			<script type="text/javascript" src="assets/js/searching.js"></script>
	</body>
</html>