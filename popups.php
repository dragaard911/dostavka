
		<div class="popup_block">
			<div class="popup_block__bg"></div>
			<div class="popup_block__feedback popup_block__form flex column">
				<form id="feedback" class="flex column">
					<p>Связаться с нами</p>
					<p>Оставь свой комментарий и мы свяжемся с вами в ближайшее время</p>
					<div class="input_item"><input id="email" type="text"><label for="email">Электронная почта</label></div>
					<div class="input_item textarea"><textarea id="comment" name="comment"></textarea><label for="comment">Комментарий</label></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Отправить</button>
						<button type="button" class="flex white close"><span>ОТмена</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_success.svg">
						<p>Заяка отправлена</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__pass popup_block__form flex column">
				<form id="passchange" class="flex column">
					<p>Изменить пароль</p>
					<div class="input_item"><input id="pass" type="password"><label for="pass">Придумайте новый пароль</label><img class="pass_click check_pass" src="assets/images/block13_check_pass.svg"><img class="pass_click hide_pass" src="assets/images/block13_hide_pass.svg"></div>
					<div class="input_item"><input id="pass_rep" type="password"><label for="pass_rep">Повторите пароль</label><img class="pass_click check_pass" src="assets/images/block13_check_pass.svg"><img class="pass_click hide_pass" src="assets/images/block13_hide_pass.svg"></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">сохранить пароль</button>
						<button type="button" class="flex white close"><span>ОТмена</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Пароль успешно сохранен</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__phonecall popup_block__form flex column">
				<form id="phonecall" class="flex column">
					<p>Заказать звонок</p>
					<p>Оставьте свой номер телефона и мы перезвоним в ближайшее время</p>
					<div class="input_item"><input id="name" type="text"><label for="name">Имя</label></div>
					<div class="input_item"><input id="phone" type="text"><label for="phone">Номер телефона</label></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Перезвонить мне</button>
						<button type="button" class="flex white close"><span>ОТмена</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_success.svg">
						<p>Заяка отправлена</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__feedbac popup_block__form flex column">
				<form id="phone_feedback" class="flex column">
					<p>Обратная связь</p>
					<p>Оставьте свой номер телефона и мы перезвоним в ближайшее время</p>
					<div class="flex row form_row">
						<div class="input_item"><input id="feed_name" type="text"><label for="feed_name">Имя</label></div>
						<div class="input_item"><input id="feed_phone" type="text"><label for="feed_phone">Телефон</label></div>
					</div>
					<div class="input_item"><input id="feed_email" type="text"><label for="feed_email">Электронная почта</label></div>
					<div class="input_item textarea"><textarea id="message" name="message"></textarea><label for="message">Сообщение</label></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Отправить</button>
						<button type="button" class="flex white close"><span>ОТмена</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_success.svg">
						<p>Заяка отправлена</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__bill popup_block__form flex column">
				<form id="bill" class="flex column">
					<p>Счет</p>
					<p>Счет успешно сформирован, оплатите его и мы зачислим сумму на ваш баланс.</p>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send"><img src="assets/images/block10_download.svg">Скачать счет в pdf</button>
						<button type="button" class="flex white close"><span>ОТмена</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Скачивание началось</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__error popup_block__form flex column">
				<form id="error" class="flex column">
					<p>Ошибка</p>
					<p>На вашем счету недостаточно средств для офомления заявки. Пожалуйста, пополните баланс и создайте заявку.</p>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Пополнить баланс</button>
						<button type="button" class="flex white close"><span>Закрыть</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Blank success</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__adress popup_block__form flex column">
				<form id="adress" class="flex column">
					<p>Добавить адрес</p>
					<p>Добавить адрес в книгу адресов?</p>
					<div class="input_item"><input id="index_adress" type="text"><label for="index_adress">Индекс</label></div>
					<div class="input_item"><input id="adress_adress" type="text"><label for="adress_adress">Адрес</label></div>
					<div class="input_item"><input id="company_adress" type="text"><label for="company_adress">Название компании</label></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">добавить адрес</button>
						<button type="button" class="flex white close"><span>Закрыть</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Адрес успешно добавлен</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__adress_change popup_block__form flex column">
				<form id="adress_change" class="flex column">
					<p>Изменить адрес</p>
					<div class="input_item"><input id="index_adress_ch" type="text"><label for="index_adress_ch">Индекс</label></div>
					<div class="input_item"><input id="adress_adress_ch" type="text"><label for="adress_adress_ch">Адрес</label></div>
					<div class="input_item"><input id="company_adress_ch" type="text"><label for="company_adress_ch">Название компании</label></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Изменить адрес</button>
						<button type="button" class="flex white close"><span>Закрыть</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Адрес успешно изменен</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__contact popup_block__form flex column">
				<form id="contact" class="flex column">
					<p>Добавить контакт</p>
					<p>Добавить контакт в адресную книгу?</p>
					<div class="input_item"><input id="name_contact" type="text"><label for="name_contact">ФИО</label></div>
					<div class="input_item"><input id="phone_contact" type="text"><label for="phone_contact">Телефон</label></div>
					<div class="input_item"><input id="company_contact" type="text"><label for="company_contact">Компания</label></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">добавить контакт</button>
						<button type="button" class="flex white close"><span>Закрыть</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Контакт успешно добавлен</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__contact_change popup_block__form flex column">
				<form id="contact_change" class="flex column">
					<p>Изменить контакт</p>
					<div class="input_item"><input id="name_contact_ch" type="text"><label for="name_contact_ch">ФИО</label></div>
					<div class="input_item"><input id="phone_contact_ch" type="text"><label for="phone_contact_ch">Телефон</label></div>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Изменить контакт</button>
						<button type="button" class="flex white close"><span>Закрыть</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Контакт успешно изменен</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__order popup_block__form flex column">
				<form id="order" class="flex column">
					<img src="assets/images/form_pass.svg">
					<p>Заказ принят</p>
					<button type="button" class="flex blue close"><span>Личный кабинет</span></button>
				</form>
			</div>
			<div class="popup_block__del_contact popup_block__form flex column">
				<form id="del_contact" class="flex column">
					<p>Удалить контакт</p>
					<p>Вы действительно хотите удалить контакт из адресной книги?</p>
					<p>Константинопольский Константин Константинович</p>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Удалить контакт</button>
						<button type="button" class="flex white close"><span>Закрыть</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Контакт успешно удален</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__del_adress popup_block__form flex column">
				<form id="del_adress" class="flex column">
					<p>Удалить адрес</p>
					<p>Вы действительно хотите удалить адрес?</p>
					<p>Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send">Удалить адрес</button>
						<button type="button" class="flex white close"><span>Закрыть</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Адрес успешно удален</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__fullwidth popup_adress">
				<div class="center flex column">
					<p>Выберите адрес из сохранённых адресов</p>
					<div class="input_item"><input id="adress_search" type="text"><label for="adress_search">Поиск по индексу, адресу, компании<img src="assets/images/popup_search.svg"></label></div>
					<div class="popup_table offset">
						<div class="popup_table_th">
							<div>Индекс</div>
							<div>Адрес</div>
							<div>Компания</div>
							<div></div>
						</div>
						<div class="overflow_table">
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>Индекс</span>426000</div>
								<div><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="popup_buttons">
					<div class="center flex">
						<button type="button" class="flex yellow inactive"><span>Выбрать контакт</span></button>
						<button type="button" class="flex white close"><span>отмена</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__fullwidth popup_contacts">
				<div class="center flex column">
					<p>Выберите контакт из сохраненных контактов</p>
					<div class="input_item"><input id="contacts_search" type="text"><label for="contacts_search">Поиск по ФИО и телефону<img src="assets/images/popup_search.svg"></label></div>
					<div class="popup_table offset">
						<div class="popup_table_th">
							<div>ФИО</div>
							<div>Телефон</div>
							<div>Компания</div>
							<div></div>
						</div>
						<div class="overflow_table">
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
							<div class="popup_table_td">
								<div><span>ФИО</span>Константинопольский Константин Константинович</div>
								<div><span>Телефон</span>+7 999 587 54 85</div>
								<div><span>Компания</span>Комсомольск-на-Амуре Логистик Груп Инкорпорейтед</div>
								<div><a href="#"><img src="assets/images/popup_change.svg"></a><a href="#"><img src="assets/images/popup_remove.svg"></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="popup_buttons">
					<div class="center flex">
						<button type="button" class="flex yellow inactive"><span>Выбрать контакт</span></button>
						<button type="button" class="flex white close"><span>отмена</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__fullwidth popup_maps">
				<div class="center flex column">
					<p>Выберите адрес на карте</p>
				</div>
				<div class="popup_map">
					<div id="map" style="width: 100%;"></div>
				</div>
				<div class="popup_buttons">
					<div class="center flex">
						<p id="adress_line">458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
						<button type="button" class="flex yellow inactive"><span>Выбрать адрес</span></button>
						<button type="button" class="flex white close"><span>отмена</span></button>
					</div>
				</div>
			</div>
			<div class="popup_block__nakl popup_block__form flex column">
				<form id="nakl" class="flex column">
					<p>Накладная</p>
					<div class="popup_block__feedback_buttons flex">
						<button type="button" class="flex yellow send"><img src="assets/images/block10_download.svg">Скачать в pdf</button>
						<button type="button" class="flex white close"><span>ОТмена</span></button>
					</div>
				</form>
				<div class="result">
					<div class="result__success">
						<img src="assets/images/form_pass.svg">
						<p>Скачивание началось</p>
						<button type="button" class="flex blue close"><span>закрыть</span></button>
					</div>
				</div>
			</div>
		</div>