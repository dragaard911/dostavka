<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_history_order.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_history_order_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block11">
				<div class="center column">
					<div class="block11__row flex">
						<p>Заказ № RU180312-145877</p>
						<button type="button" class="flex white"><img src="assets/images/block6_arrow.svg">К списку заказов</button>
					</div>
					<div class="block11_label confirm">Заказ одобрен оператором и доступен курьерам</div>
					<div class="block11__info">
						<div class="flex sendrecieve">
							<div class="flex column">
								<p><img src="assets/images/block8_sender.svg">Отправитель</p>
								<div>
									<p><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
									<p><span>фио</span>Константинопольский Константин Константинович</p>
									<p><span>Телефон</span>+7 999 666 98 45</p>
									<p><span>дата и время забора</span>20 сентября, 10:00—20:00</p>
									<p><span>Компания</span>Ростелеком</p>
								</div>
							</div>
							<div class="flex column">
								<p><img src="assets/images/block8_recieve.svg">Получатель</p>
								<div>
									<p><span>Адрес</span>Комсомольск-на-Амуре, 10 лет Октября, 34-80</p>
									<p><span>фио</span>Константинопольский Константин Константинович</p>
									<p><span>Телефон</span>+7 999 666 98 45</p>
									<p><span>дата доставки</span>22 сентября</p>
									<p><span>Компания</span>Yodiz Studio</p>
								</div>
							</div>
						</div>
						<div class="delivery">
							<p>Кто доставляет</p>
							<div class="flex">
								<div class="flex">
									<p><span>Транспортная компания</span>Major Delivery</p>
									<p><span>номер накладной</span>RU180312-145877</p>
									<p><span>Стоимость</span>478 555 руб.</p>
								</div>
								<button type="button" class="flex blue"><img src="assets/images/block10_download.svg">скачать накладную</button>
							</div>
						</div>
						<div class="flex column goods">
							<p>Информация о грузе</p>
							<div class="flex column">
								<ul class="flex">
									<li><p><span>Вес</span>145,5 кг.</p></li>
									<li><p><span>ширина</span>185,5 см.</p></li>
									<li><p><span>Высота</span>185 см.</p></li>
									<li><p><span>глубина</span>20 см.</p></li>
								</ul>
								<p><span>Описание</span>Ящик деревянный, белого цвета, внутри хрупкое, соответствующие обозначения на упаковке есть.</p>
								<p><span>Комментарий</span>Позвонить за 1 час до прибытия на адрес</p>
							</div>
						</div>
						<div class="status flex column">
							<p>История заказа</p>
							<div class="status_table flex column">
								<div class="status_table_th flex">
									<div>дата<img src="assets/images/block11_arrow.svg"></div>
									<div>Описание<img src="assets/images/block11_arrow.svg"></div>
								</div>
								<div class="status_table_td flex">
									<div><span>дата</span>25.09.2020</div>
									<div><span>Описание</span>Вы создали заказ. Ящик деревянный, белого цвета, внутри хрупкое, соответствующие обозначения на упаковке есть. Позвонить за 1 час до прибытия на адрес</div>
								</div>
								<div class="status_table_td flex">
									<div><span>дата</span>26.09.2020</div>
									<div><span>Описание</span>Заказ оплачен</div>
								</div>
								<div class="status_table_td flex">
									<div><span>дата</span>27.09.2020</div>
									<div><span>Описание</span>Заказ принят транспортной компанией</div>
								</div>
								<div class="status_table_td flex">
									<div><span>дата</span>28.09.2020</div>
									<div><span>Описание</span>Заказ доставлен</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>