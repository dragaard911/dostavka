<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_balance.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_balance_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block12">
				<div class="center column">
					<div class="block12__row flex">
						<p>Личный кабинет</p>
					</div>
					<div class="block12__balance flex column">
						<p>Пополнить баланс</p>
						<div class="flex">
							<div class="flex column">
								<p>Введите сумму, мы сформируем счет, после чего его надо будет оплатить.</p>
								<div class="block12__balance_row flex">
									<div class="input_item"><input id="summ" type="text"><label for="summ">Сумма пополнения</label></div>
									<button type="button" class="flex yellow">сформировать счет</button>
								</div>
							</div>
							<div class="block12__balance_summ">
								<p><span>Баланс</span>4 585 456 руб.</p>
							</div>
						</div>
					</div>
					<div class="block12__balance_table">
						<p>Движение средств</p>
							<div class="balance_table flex column">
								<div class="balance_table_th flex">
									<div>дата</div>
									<div>сумма</div>
									<div>основание</div>
									<div>баланс</div>
								</div>
								<div class="balance_table_td flex">
									<div><p><span>дата</span>25.09.2020</p></div>
									<div><p><span>сумма</span>458 569</p></div>
									<div><p><span>основание</span>Счет № RU180312-145877</p></div>
									<div><p><span>баланс</span>458 569</p></div>
								</div>
								<div class="balance_table_td flex">
									<div><p><span>дата</span>25.09.2020</p></div>
									<div><p><span>сумма</span>458 569</p></div>
									<div><p><span>основание</span>Счет № RU180312-145877</p></div>
									<div><p><span>баланс</span>458 569</p></div>
								</div>
								<div class="balance_table_td flex">
									<div><p><span>дата</span>25.09.2020</p></div>
									<div><p><span>сумма</span>458 569</p></div>
									<div><p><span>основание</span>Счет № RU180312-145877</p></div>
									<div><p><span>баланс</span>458 569</p></div>
								</div>
								<div class="balance_table_td flex">
									<div><p><span>дата</span>25.09.2020</p></div>
									<div><p><span>сумма</span>458 569</p></div>
									<div><p><span>основание</span>Счет № RU180312-145877</p></div>
									<div><p><span>баланс</span>458 569</p></div>
								</div>
							</div>
							<div class="balance_pagination flex">
								<button type="button" class="flex blue">Следующая страница</button>
								<ul class="flex">
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li>...</li>
									<li><a href="#">10</a></li>
								</ul>
							</div>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>