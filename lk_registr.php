<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_registr.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_registr_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk_noauth.php');?>
			<section class="block13">
				<div class="center column">
					<div class="block13__row flex">
						<p>Регистрация</p>
						<button type="button" class="flex blue">у меня есть аккаунт</button>
					</div>
					<div class="block13__info flex column">
						<div class="block13__reg_block">
							<p><span><span>1</span><img src="assets/images/block13_success.svg"></span>Контактные данные</p>
							<form id="reg_step1" name="reg_step1">
								<div class="flex column input_block">
									<div class="flex">
										<div class="input_item"><input id="reg_fname" type="text" name="reg_fname"><label for="reg_fname">Фамилия</label></div>
										<div class="input_item"><input id="reg_name" type="text" name="reg_name"><label for="reg_name">Имя</label></div>
									</div>
									<div class="flex">
										<div class="input_item"><input id="reg_email" type="text" name="reg_email"><label for="reg_email">E-mail</label></div>
										<div class="input_item"><input id="reg_phone" type="text" name="reg_phone"><label for="reg_phone">Телефон</label></div>
									</div>
								</div>
							</form>
						</div>
						<div class="block13__reg_block">
							<p><span><span>2</span><img src="assets/images/block13_success.svg"></span>Пароль</p>
							<form id="reg_step2" name="reg_step2">
								<div class="flex column input_block">
									<p>Длина пароля не менее 6 символов. Использовать минимум 1 цифру.</p>
									<div class="flex">
										<div class="input_item"><input id="reg_passwd" name="reg_passwd" type="password" autocomplete="off"><label for="reg_passwd">Пароль</label><img class="pass_click check_pass" src="assets/images/block13_check_pass.svg"><img class="pass_click hide_pass" src="assets/images/block13_hide_pass.svg"></div>
										<div class="input_item"><input id="reg_passwd_re" name="reg_passwd_re" type="password" autocomplete="off"><label for="reg_passwd_re">Повторите пароль</label><img class="pass_click check_pass" src="assets/images/block13_check_pass.svg"><img class="pass_click hide_pass" src="assets/images/block13_hide_pass.svg"></div>
									</div>
									<div class="flex error_flex">
										<p class="error_field passwd_difficult">Сложность пароля: <span>низкий</span></p>
										<p class="error_field passwd_difference"><span>Пароли совпадают</span></p>
									</div>
								</div>
							</form>
						</div>
						<div class="block13__reg_block">
							<p><span><span>3</span><img src="assets/images/block13_success.svg"></span>О компании</p>
							<form id="reg_step3" name="reg_step3">
								<div class="flex column input_block">
									<p>Сервис сам находит данные по организации - достаточно ввести ИНН, БИК банка и Р/С</p>
									<div class="flex">
										<div class="input_item"><input id="reg_inn" type="text" name="reg_inn"><label for="reg_inn">ИНН</label></div>
										<div class="input_item"><input id="reg_kpp" type="text" name="reg_kpp"><label for="reg_kpp">КПП</label></div>
									</div>
									<div class="flex fullwidth bottom">
										<div class="input_item"><input id="reg_ogrn" type="text" name="reg_ogrn"><label for="reg_ogrn">ОГРН</label></div>
									</div>
									<div class="flex">
										<div class="input_item"><input id="reg_bik" type="text" name="reg_bik"><label for="reg_bik">Бик Банка</label></div>
										<div class="input_item"><input id="reg_bank" type="text" name="reg_bank"><label for="reg_bank">Банк</label></div>
									</div>
									<div class="flex bottom">
										<div class="input_item"><input id="reg_ras_bill" type="text" name="reg_ras_bill"><label for="reg_ras_bill">Рас. счет</label></div>
										<div class="input_item"><input id="reg_kor_bill" type="text" name="reg_kor_bill"><label for="reg_kor_bill">Корр. счет</label></div>
									</div>
									<div class="flex fullwidth">
										<div class="input_item"><input id="reg_comp" type="text" name="reg_comp"><label for="reg_comp">Название компании</label></div>
									</div>
									<div class="flex fullwidth">
										<div class="input_item"><input id="reg_director" type="text" name="reg_director"><label for="reg_director">Генеральный директор</label></div>
									</div>
									<div class="flex fullwidth adress_original">
										<div class="input_item"><input id="reg_com_adress" type="text" name="reg_com_adress"><label for="reg_com_adress">Юридический адрес</label></div>
									</div>
									<div class="flex">
										<p class="check_string"><input type="checkbox" id="reg_adress_copy" checked><label for="reg_adress_copy"><span><img src="assets/images/form_checkbox.svg"></span>Совпадает с фактическим</label></p>
									</div>
									<div class="flex fullwidth adress_copy">
										<div class="input_item"><input id="reg_usr_adress" type="text"><label for="reg_usr_adress">Фактический адрес</label></div>
									</div>
								</div>
							</form>
						</div>
						<button type="button" class="flex yellow inactive">зарегистрироваться</button>
						<p class="block13__info_p">Нажимая кнопку «Зарегистрироваться», вы принимаете условия <a href="#">Договора оферты</a> и <a href="#">Соглашения о неразглашении</a>.</p>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>