<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/main.css" rel="stylesheet" type="text/css">
		<link href="assets/css/main_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
		<script src="https://api-maps.yandex.ru/2.1/?apikey=caaff12d-84d6-41c7-b12f-8e434af75542&lang=ru_RU" type="text/javascript"></script>
    </head>
	<body>
		<?php include('popups.php');?>
		<div class="container">
			<?php include('header.php');?>
			<section class="block1">
				<div class="center column">
					<ul>
						<li>
							<span><img src="assets/images/block1_icon1.svg"></span>
							<p>Лучшие транспортные компании на рынке</p>
						</li>
						<li>
							<span><img src="assets/images/block1_icon2.svg"></span>
							<p>Один договор более 5 транспортных компаний</p>
						</li>
						<li>
							<span><img src="assets/images/block1_icon3.svg"></span>
							<p>Доставка «Дверь в дверь»</p>
						</li>
						<li>
							<span><img src="assets/images/block1_icon4.svg"></span>
							<p>Более выгодные тарифы транспортных компаний</p>
						</li>
						<li>
							<span><img src="assets/images/block1_icon5.svg"></span>
							<p>Более 10 лет опыта на рынке транспортных услуг</p>
						</li>
						<li>
							<span><img src="assets/images/block1_icon6.svg"></span>
							<p>Самая быстрая или самая экономичная доставка</p>
						</li>
					</ul>
					
					<video class="block1__girl" poster="assets/images/block1_girl.svg" width="837" height="1002" autoplay muted loop>
					  <source src="assets/images/woman.webm" type='video/webm; codecs="vp8, vorbis"'>
					  <img class="block1__girl" width="837" height="1002" src="assets/images/block1_girl.svg">
					</video>
					<!--<img class="block1__girl" width="837" height="1002" src="assets/images/block1_girl.svg">-->
				</div>
			</section>
		</div>
			<section class="block2">
				<div class="center">
					<div class="block2__left_side">
						<h2>Как это работает</h2>
						<p>Заполните поля и мы скажем ориентировочные сроки и стоимость доставки</p>
						<div class="block2__adress_block flex">
							<div class="block2__adress_block_adresses flex column">
								<div class="block2__adress_block_container">
									<div class="block2__border_block flex">
										<div class="input_container">
											<label for="from">Адрес отправления полностью<span class="error_text">Неверный адрес</span></label>
											<div contenteditable="true" id="from"></div>
											<input type="hidden" name="from">
										</div>
										<div class="buttons">
											<button type="button" class="map_list"><img src="assets/images/point.svg"></button>
										</div>
									</div>
									<div class="adress_list recent">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
									<div class="adress_list book">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
								</div>
								<div class="separate"><hr></div>
								<div class="block2__adress_block_container">
									<div class="block2__border_block flex">
										<div class="input_container">
											<label for="to">Адрес отправления полностью<span class="error_text">Неверный адрес</span></label>
											<div contenteditable="true" id="to"></div>
											<input type="hidden" name="to">
										</div>
										<div class="buttons">
											<button type="button" class="map_list"><img src="assets/images/point.svg"></button>
										</div>
									</div>
									<div class="adress_list recent">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
									<div class="adress_list book">
										<div class="adress_list__row flex">
											<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
										</div>
										<ul>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
											<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="block2__delivery_block">
							<p>Стоимость будет точнее, если заполнить габариты груза</p>
							<div class="block2__delivery_block__info flex">
								<div>
									<p>Вес груза<span>кг</span></p>
									<div class="input_container">
										<div class="input_item"><input id="weight" type="text"><label for="weight">100</label></div>
									</div>
								</div>
								<div>
									<p>габариты груза<span>см</span></p>
									<div class="input_container input33">
										<div class="input_item"><input id="width" type="text"><label for="width">Ширина</label></div>
										<div class="input_item"><input id="height" type="text"><label for="height">Высота</label></div>
										<div class="input_item"><input id="depth" type="text"><label for="depth">Глубина</label></div>
									</div>
								</div>
							</div>
						</div>
						<button type="button" class="flex yellow"><img src="assets/images/button_box.svg">найти лучшее предложение</button>
					</div>
					<div class="block2__right_side">
						<div>
							<img src="assets/images/block2_box.svg">
							<p>Сервис работает по принципу подбора самого быстрого или экономичного исполнителя.</p>
							<p>Иногда в одном исполнителе сочетаются оба качества.</p>
						</div>
					</div>
				</div>
			</section>	
			<section class="block3">
				<div class="center column">
					<h2>Часто задаваемые вопросы</h2>
					<div class="block3__columns flex">
						<ul class="block3__columns_list">
							<li>
								<label for="faq_1">Кто такая Капитан Доставка?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Капитан Доставка - это надежный, профессиональный и ответственный супергерой на рынке курьерских услуг. на помогает своим клиентам воспользоваться лучшими предложениями курьерских компаний для перевозки грузов</p>
								</div>
							</li>
							<li>
								<label for="faq_2">2. Чем сервис Капитан Доставка отличается от других курьерских компаний?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Капитан Доставка анализирует и предлагает лучшие предложения от лучших курьерских компании на рынке.</p>
								</div>
							</li>
							<li>
								<label for="faq_3">3. В чем уникальность сервиса?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Сервис предлагает Вам два варианта доставки «Самый быстрый» и «Самый экономичный», на основе предложений представленных курьерских компаний. Также вы можете выбрать любой вариант из предложных, в случае необходимости.</p>
								</div>
							</li>
							<li>
								<label for="faq_4">4. Капитан Доставка работает только с юридическими лицами?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Да, на данный момент сервис, работает только с юридическими лицами или ИП. В ближайшем будущем мы планируем открыть доступ для физических лиц.</p>
								</div>
							</li>
							<li>
								<label for="faq_5">5. Почему в Вашем Сервисе отсутствуют другие курьерские компании? <span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Сервис анализирует только лучшие курьерские компании на рынке. Более чем 10 летний опыт работы с курьерскими компаниями позволяет сервису Капитан Доставка гарантировать доставку ваших заказов в указанные сроки за оптимальную оплату.</p>
								</div>
							</li>
							<li>
								<label for="faq_6">6. Почему я должен выбрать именно Ваш сервис?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Капитан Доставка обладает несколькими преимуществами относительно других сервисов или услуг курьерских компаний: </p>
									<p>1. Вы оформляете договор и пользуетесь услугами более пяти лучших курьерских компаний. </p>
									<p>2. Сервис предлагает «Самый быстрый» или «Самый экономичный» вариант перевозки для вашего заказа.</p>
									<p>3. Вы получаете более выгодные тарифы на перевозку по сравнению с базовыми тарифами курьерских компаний. </p>
								</div>
							</li>
							<li>
								<label for="faq_7">7. Как пополнить баланс?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Зарегистрируйтесь на сайте, кликните на значение баланса, введите сумму пополнения и нажмите кнопку «Сформировать счет». Полученный счет оплатите с расчетного счета Вашей организации. После оплаты деньги будут зачислены на Ваш баланс на следующей рабочий день. </p>
								</div>
							</li>
							<li>
								<label for="faq_8">8. Как передать отправление курьеру?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>Вам достаточно оформить заявку, распечатать накладную и дождаться приезда курьера в указанный день. Если у Вас нет возможности распечатать накладную, сообщите об этом курьеру, он предоставит Вам бланк накладной, которую Вы сразу сможете заполнить при нём. </p>
								</div>
							</li>
							<li>
								<label for="faq_9">9. Что делать если возникла проблема?<span><img src="assets/images/button_arrow_down.svg"></span></label>
								<div class="block3__columns_list_description">
									<hr>
									<p>По любым вопросам свяжитесь с нами по адресу <a href="mailto:info@capitandelivery.ru">info@capitandelivery.ru</a> или по телефону <a href="tel:79261546298">+7(926) 154-62-98</a></p>
								</div>
							</li>
						</ul>
						<div class="block3__columns_info">
							<img src="assets/images/block3_girl.svg">
							<div class="flex column">
								<p>Не нашел ответа на вопрос? Задай его мне и я отвечу.</p>
								<p><a href="#">задать вопрос</a></p>
							</div>
						</div>
					</div>
				</div>
			</section>	
			<section class="block4">
				<div class="center column">
					<div class="block4__info flex">
						<div>
							<h2>Партнеры и клиенты</h2>
							<p>Мы используем нашу экспертизу и вовлеченность, чтобы помогать нашим клиентам и партнерам оставаться лидерами или достигать новых вершин.</p>
						</div>
						<button type="button" class="flex yellow"><img src="assets/images/button_arrow_right_b.svg">стать нашим клиентом</button>
					</div>
					<ul class="flex">
						<li><img src="assets/images/block4_logo1.svg"></li>
						<li><img src="assets/images/block4_logo2.svg"></li>
						<li><img src="assets/images/block4_logo3.svg"></li>
						<li><img src="assets/images/block4_logo4.svg"></li>
						<li><img src="assets/images/block4_logo5.svg"></li>
						<li><img src="assets/images/block4_logo6.svg"></li>
						<li><img src="assets/images/block4_logo7.svg"></li>
					</ul>
				</div>
			</section>
			<section class="block5">
				<div class="center">
					<div class="block5__info flex column">
						<p>Один сервис. Один договор. Лучшие транспортные компании</p>
						<button type="button" class="flex black"><img src="assets/images/button_arrow_right.svg">зарегистрироваться</button>
					</div>
					<img class="drone" src="assets/images/block5_drone.svg">
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>