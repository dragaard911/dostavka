			
			<footer>
				<div class="center column">
					<div class="flex footer__info">
						<div class="flex column">
							<div class="flex column">
								<p><a href="tel:+78008887766">+7(800) 888-77-66</a></p>
								<p class="phone_form"><a href="#">заказать звонок</a></p>
							</div>
							<button type="button" class="flex blue"><img src="assets/images/button_letter.svg">написать нам</button>
						</div>
						<div class="flex">
							<ul class="flex column">
								<li><a href="#">О сервисе</a></li>
								<li><a href="#">Как это работает</a></li>
								<li><a href="#">Вопрос-ответ</a></li>
								<li><a href="#">Партнеры</a></li>
							</ul>
							<ul class="flex column">
								<li><a href="#">Оферта</a></li>
								<li><a href="#">Условия использования</a></li>
								<li><a href="#">Политика конфиденциальности</a></li>
							</ul>
						</div>
						<div>
							<p><a href="#">Facebook</a></p>
							<p><a href="#">Twitter</a></p>
						</div>
						<div>
							<p><a href="#">LinkedIn</a></p>
							<p><a href="#">Instagram</a></p>
						</div>
					</div>
					<hr>
					<div class="footer__last_row flex">
						<p><span>Адрес центрального офиса</span>г. Комсомольск-на-Амуре, Ленинский пр. 153а, Setl Center</p>
						<p>©2020 Company name</p>
					</div>
				</div>
			</footer>
			<script type="text/javascript" src="assets/js/jquery-3.4.1.min.js"></script>
			<script type="text/javascript" src="assets/js/jquery-ui.js"></script>
			<script type="text/javascript" src="assets/js/map.js"></script>
			<script type="text/javascript" src="assets/js/scripts.js"></script>
			