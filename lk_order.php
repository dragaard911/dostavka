<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dostavka</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="assets/css/fonts.css" rel="stylesheet" type="text/css">
		<link href="assets/css/normal.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_order.css" rel="stylesheet" type="text/css">
		<link href="assets/css/lk_order_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup.css" rel="stylesheet" type="text/css">
		<link href="assets/css/popup_m.css" rel="stylesheet" type="text/css">
		<link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
		<script src="https://api-maps.yandex.ru/2.1/?apikey=caaff12d-84d6-41c7-b12f-8e434af75542&lang=ru_RU" type="text/javascript"></script>
    </head>
	<body>
		<?php include('popups.php');?>
			<?php include('header_lk.php');?>
			<section class="block15">
				<div class="center column">
					<div class="block15__row flex">
						<p>Оформление заказа</p>
					</div>
					<div class="block2__adress_block flex column block15__reg_block">
							<p><span><span>1</span><img src="assets/images/block13_success.svg"></span>Адреса</p>
							<form id="step1" name="step1" class="flex">
									<div class="block2__adress_block_adresses flex column">
										<div class="block2__adress_block_container">
											<div class="block2__border_block flex">
												<div class="input_container">
													<label for="from">Адрес отправления полностью<span class="error_text">Неверный адрес</span></label>
													<div contenteditable="true" id="from"></div>
													<input type="hidden" name="from">
												</div>
												<div class="buttons flex">
													<button type="button" class="check_list"><img src="assets/images/list.svg"></button>
													<button type="button" class="map_list"><img src="assets/images/point.svg"></button>
												</div>
											</div>
											<div class="adress_list recent">
												<div class="adress_list__row flex">
													<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
													<button type="button" class="flex white shitchbook check_list"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
													<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
												</div>
												<ul>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
												</ul>
											</div>
											<div class="adress_list book">
												<div class="adress_list__row flex">
													<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
													<button type="button" class="flex white shitchbook"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
													<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
												</div>
												<ul>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
												</ul>
											</div>
										</div>
										<div class="separate"><hr></div>
										<div class="block2__adress_block_container">
											<div class="block2__border_block flex">
												<div class="input_container">
													<label for="to">Адрес отправления полностью<span class="error_text">Неверный адрес</span></label>
													<div contenteditable="true" id="to"></div>
													<input type="hidden" name="to">
												</div>
												<div class="buttons flex">
													<button type="button" class="check_list"><img src="assets/images/list.svg"></button>
													<button type="button" class="map_list"><img src="assets/images/point.svg"></button>
												</div>
											</div>
											<div class="adress_list recent">
												<div class="adress_list__row flex">
													<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
													<button type="button" class="flex white check_list"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
													<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
												</div>
												<ul>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
												</ul>
											</div>
											<div class="adress_list book">
												<div class="adress_list__row flex">
													<button type="button" class="flex white map_list"><span><img src="assets/images/list_point.svg">Указать на карте</span></button>
													<button type="button" class="flex white check_list"><span><img src="assets/images/list_contact.svg">Выбрать из адресной книги</span></button>
													<button type="button" class="flex white add_book"><span><img src="assets/images/add_adress.svg">Добавить в адресную книгу</span></button>
												</div>
												<ul>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
													<li><span>458000, Комсомольск-на-Амуре, 10 лет Октября, 34-80</span></li>
												</ul>
											</div>
										</div>
									</div>
								<div class="block15__short_info">
									<div class="short_info">
										<p><span>Отправитель</span>Major Delivery</p>
										<p><span>Срок доставки</span>10 дней</p>
										<p><span>Стоимость доставки</span>401 585 руб.</p>
									</div>
									<div class="short_attention">
										<p><span>Внимание!</span>В случае изменения/ уточнения параметров заказа, стоимость может изменится.</p>
									</div>
								</div>
							</form>
						</div>
					<div class="block15__info flex column step_two">
						<div class="block15__reg_block">
							<p><span><span>2</span><img src="assets/images/block13_success.svg"></span>Получатель</p>
							<form id="step2" name="step2">
								<div class="flex column input_block">
									<div class="flex">
										<div class="input_item"><input id="get_name" type="text" name="get_name"><label for="get_name">ФИО</label></div>
										<div class="input_item"><input id="get_phone" type="text" name="get_phone"><label for="get_phone">Телефон</label></div>
									</div>
									<div class="flex">
										<div class="input_item get_date"><input id="get_date" type="text" name="get_date"><label for="get_date">Дата забора</label></div>
										<select id="get_time" name="get_time">
											<option select value="11">1:00 - 2:00</option>
											<option value="22">5:00 - 8:00</option>
											<option value="33">10:00 - 20:00</option>
										</select>
									</div>
									<div class="flex fullwidth bottom">
										<div class="input_item"><input id="get_company" type="text" name="get_company"><label for="get_company">Компания (необязательно)</label></div>
									</div>
									<div class="adress_book_buttons flex fullwidth bottom">
										<a href="#" class="add_adress"><img src="assets/images/add_adress.svg"><span>Добавить в книгу контактов</span></a>
										<a href="#" class="get_adress"><img src="assets/images/get_adress.svg"><span>Выбрать из книги контактов</span></a>
									</div>
								</div>
							</form>
						</div>
						<div class="block15__reg_block">
							<p><span><span>3</span><img src="assets/images/block13_success.svg"></span>Отправитель</p>
							<form id="step3" name="step3">
								<div class="flex column input_block">
									<div class="flex">
										<div class="input_item"><input id="set_name" type="text" name="set_name"><label for="set_name">ФИО</label></div>
										<div class="input_item"><input id="set_phone" type="text" name="set_phone"><label for="set_phone">Телефон</label></div>
									</div>
									<div class="flex fullwidth bottom">
										<div class="input_item"><input id="set_company" type="text" name="set_company"><label for="set_company">Компания (необязательно)</label></div>
									</div>
									<div class="adress_book_buttons flex fullwidth bottom">
										<a href="#" class="add_adress"><img src="assets/images/add_adress.svg"><span>Добавить в книгу контактов</span></a>
										<a href="#" class="get_adress"><img src="assets/images/get_adress.svg"><span>Выбрать из книги контактов</span></a>
									</div>
								</div>
							</form>
						</div>
						<div class="block15__reg_block">
							<p><span><span>4</span><img src="assets/images/block13_success.svg"></span>Информация о грузе</p>
							<form id="step4" name="step4">
								<div class="block2__delivery_block">
									<p>Стоимость будет точнее, если заполнить габариты груза</p>
									<div class="block2__delivery_block__info flex">
										<div>
											<p>Вес груза<span>кг</span></p>
											<div class="input_container">
												<div class="input_item"><input id="weight" type="text" name="weight"><label for="weight">100</label></div>
											</div>
										</div>
										<div>
											<p>габариты груза<span>см</span></p>
											<div class="input_container input33 flex">
												<div class="input_item"><input id="width" type="text" name="width"><label for="width">Ширина</label></div>
												<div class="input_item"><input id="height" type="text" name="height"><label for="height">Высота</label></div>
												<div class="input_item"><input id="depth" type="text" name="depth"><label for="depth">Глубина</label></div>
											</div>
										</div>
									</div>
									<div class="flex fullwidth bottom">
										<div class="input_item"><input id="goods" type="text" name="goods"><label for="goods">Описание груза</label></div>
									</div>
									<div class="flex fullwidth bottom">
										<div class="input_item"><input id="order_comment" type="text" name="order_comment"><label for="order_comment">Комментарий</label></div>
									</div>
								</div>
							</form>
						</div>
						<button type="button" class="flex yellow inactive">оформить заказ</button>
					</div>
				</div>
			</section>	
			<?php include('footer.php');?>
	</body>
</html>